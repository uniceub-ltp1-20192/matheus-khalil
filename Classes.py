class Pessoa:
    def __init__(self,nome,idade,tamanho):
        self._nome    = nome
        self._idade   = idade
        self._tamanho = tamanho
    
    @property
    def tamanho (self):
        return self._tamanho
    
    @tamanho.setter    
    def tamanho(self,tamanho):
        self._tamanho = tamanho
    
   
    @property
    def nome (self):
        return self._nome
    
    @nome.setter    
    def nome(self,nome):
        self._nome = nome
    
   
    @property
    def idade (self):
        return self._idade
    
    @idade.setter    
    def idade(self,idade):
        self._idade = idade
    
    def falar():
        print("Oi")
    def comer():
        print("Irei comer algo")
        


p1 = Pessoa("Matheus",21,1.80)
print(p1.nome)
print(p1.idade)
print(p1.tamanho)
