for cebolas in range(1,21):
    print(cebolas)

lista = [valor for valor in range(1,1000001)]
print(min(lista))
print(max(lista))
print(sum(lista))
"""
for cebolas in range(1,1000001):
    print(cebolas)
"""

print("")

for cebolas in range(1,21,2):
    print(cebolas)

print("")

for cebolas in range(3,1001,3):
    print(cebolas)

print("")

for cebolas in range(1,101):
    print(cebolas**3)

print("")

lista = [valor**3 for valor in range(1,101)]
print(lista)
